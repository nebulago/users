package dao

type User struct {
	UID      string `gorm:"type:varchar(20);unique_index"`
	Name     string `gorm:"type:varchar(20)"`
	Email    string `gorm:"type:varchar(50);unique_index"`
	Password string
}

func (*User) TableName() string {
	return "users"
}
