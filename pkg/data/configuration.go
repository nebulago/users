package data

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"gitlab.com/nebulago/users/pkg/domain/model"
)

type databaseConfiguration struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	DBName   string `json:"dbname"`
	User     string `json:"user"`
	Password string `json:"password"`
	SSLMode  string `json:"ssl"`
	Schema   string `json:"schema"`
}

type httpConfiguration struct {
	Address string `json:"address"`
}

type configuration struct {
	DatabaseConfiguration *databaseConfiguration `json:"database"`
	HttpConfiguration     *httpConfiguration     `json:"http"`
}

type ConfigurationRepository struct {
	configuration *configuration
}

func NewConfigurationRepository(file *os.File) (*ConfigurationRepository, error) {
	byteValue, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}
	var result configuration
	err = json.Unmarshal([]byte(byteValue), &result)
	if err != nil {
		return nil, err
	}
	return &ConfigurationRepository{configuration: &result}, nil
}

func (c *ConfigurationRepository) GetConfiguration() *model.Configuration {
	result := model.Configuration{
		HttpConfiguration: &model.HttpConfiguration{Address: c.configuration.HttpConfiguration.Address},
		DatabaseConfiguration: &model.DatabaseConfiguration{
			Host:     c.configuration.DatabaseConfiguration.Host,
			Port:     c.configuration.DatabaseConfiguration.Port,
			DBName:   c.configuration.DatabaseConfiguration.DBName,
			User:     c.configuration.DatabaseConfiguration.User,
			Password: c.configuration.DatabaseConfiguration.Password,
			SSLMode:  c.configuration.DatabaseConfiguration.SSLMode,
			Schema:   c.configuration.DatabaseConfiguration.Schema,
		},
	}
	return &result
}
