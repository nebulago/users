package data

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"gitlab.com/nebulago/users/pkg/data/dao"
	"gitlab.com/nebulago/users/pkg/domain/model"
)

type Database struct {
	db *gorm.DB
}

func NewDatabase(configuration *model.DatabaseConfiguration) (*Database, *model.Error) {
	dbg := &Database{}
	var err error
	dbg.db, err = gorm.Open("postgres", fmt.Sprintf("host=%s port=%s dbname=%s search_path=%s user=%s password=%s sslmode=%s",
		configuration.Host, configuration.Port, configuration.DBName, configuration.Schema, configuration.User, configuration.Password, configuration.SSLMode))
	if err != nil {
		return nil, &model.Error{Tag: "Could not connect to database", Message: err.Error()}
	}
	dbg.db.Exec(fmt.Sprintf("CREATE SCHEMA IF NOT EXISTS %s", configuration.Schema))
	dbg.db.LogMode(true)
	dbg.db.AutoMigrate(&dao.User{})
	return dbg, nil
}

func (dbg *Database) CreateUser(user *model.User, password string) *model.Error {
	userDAO := dao.User{
		UID:      user.UID,
		Name:     user.Name,
		Email:    user.Email,
		Password: password,
	}
	// Check user not exist
	var count int
	if dbc := dbg.db.Model(&dao.User{}).Where("uid = ?", userDAO.UID).Or("email = ?", userDAO.Email).Count(&count); dbc.Error != nil {
		return &model.Error{Code: 500, Tag: "DATABASE_ERROR", Message: dbc.Error.Error()}
	}
	if count > 0 {
		return &model.Error{Code: 400, Tag: "USER_ALREADY_EXIST"}
	}
	// Create the user
	if dbc := dbg.db.Create(&userDAO); dbc.Error != nil {
		return &model.Error{Code: 500, Tag: "DATABASE_ERROR", Message: dbc.Error.Error()}
	}
	return nil
}

func (dbg *Database) GetUsersWithPrefix(prefix string, offset int, limit int) (*[]model.User, int, *model.Error) {
	var count int
	likePrefixStr := prefix + "%"
	baseQuery := dbg.db.Where("uid LIKE ?", likePrefixStr).Or("name LIKE ?", likePrefixStr).Or("email LIKE ?", likePrefixStr)

	if dbc := baseQuery.Model(&dao.User{}).Count(&count); dbc.Error != nil {
		return nil, count, &model.Error{Code: 500, Tag: "DATABASE_ERROR", Message: dbc.Error.Error()}
	}

	var users []dao.User
	if dbc := baseQuery.Offset(offset).Limit(limit).Find(&users); dbc.Error != nil {
		return nil, count, &model.Error{Code: 500, Tag: "DATABASE_ERROR", Message: dbc.Error.Error()}
	}
	var result []model.User
	for _, user := range users {
		result = append(result, model.User{UID: user.UID, Name: user.Name, Email: user.Email})
	}
	return &result, count, nil
}

func (dbg *Database) GetUsers(offset int, limit int) (*[]model.User, int, *model.Error) {
	var count int
	if dbc := dbg.db.Model(&dao.User{}).Count(&count); dbc.Error != nil {
		return nil, count, &model.Error{Code: 500, Tag: "DATABASE_ERROR", Message: dbc.Error.Error()}
	}

	var users []dao.User
	if dbc := dbg.db.Offset(offset).Limit(limit).Find(&users); dbc.Error != nil {
		return nil, count, &model.Error{Code: 500, Tag: "DATABASE_ERROR", Message: dbc.Error.Error()}
	}
	var result []model.User
	for _, user := range users {
		result = append(result, model.User{UID: user.UID, Name: user.Name, Email: user.Email})
	}
	return &result, count, nil

}

func (dbg *Database) GetUser(id string) (*model.User, *model.Error) {
	var user dao.User
	if dbc := dbg.db.Where("uid = ?", id).First(&user); dbc.Error != nil {
		return nil, &model.Error{Code: 500, Tag: "DATABASE_ERROR", Message: dbc.Error.Error()}
	}
	return &model.User{UID: user.UID, Name: user.Name, Email: user.Email}, nil
}

func (dbg *Database) UpdateUser(user *model.User) (*model.User, *model.Error) {
	var currentUser dao.User
	if dbc := dbg.db.Model(&currentUser).Where("uid = ?", user.UID).First(&currentUser); dbc.Error != nil {
		return nil, &model.Error{Code: 500, Tag: "DATABASE_ERROR", Message: dbc.Error.Error()}
	}
	currentUser.Name = user.Name
	currentUser.Email = user.Email
	// Update the user
	if dbc := dbg.db.Save(&currentUser); dbc.Error != nil {
		return nil, &model.Error{Code: 500, Tag: "DATABASE_ERROR", Message: dbc.Error.Error()}
	}
	return &model.User{UID: currentUser.UID, Name: currentUser.Name, Email: currentUser.Email}, nil
}

func (dbg *Database) DeleteUser(id string) *model.Error {
	var currentUser dao.User
	if dbc := dbg.db.Model(&currentUser).Where("uid = ?", id).First(&currentUser); dbc.Error != nil {
		return &model.Error{Code: 500, Tag: "DATABASE_ERROR", Message: dbc.Error.Error()}
	}
	// Delete the user
	if dbc := dbg.db.Delete(&currentUser); dbc.Error != nil {
		return &model.Error{Code: 500, Tag: "DATABASE_ERROR", Message: dbc.Error.Error()}
	}
	return nil

}
