package usecase

import "gitlab.com/nebulago/users/pkg/domain/model"

func (uc *UseCaseImpl) DeleteUser(id string) *model.Error {
	return uc.repository.DeleteUser(id)
}
