package usecase

import "gitlab.com/nebulago/users/pkg/domain/model"

func (uc *UseCaseImpl) GetUsers(prefix *string, offset *int, limit *int) (*[]model.User, int, *model.Error) {
	searchOffset := 0
	searchLimit := 10
	if offset != nil {
		searchOffset = *offset
	}
	if limit != nil {
		searchLimit = *limit
	}
	if prefix != nil && (*prefix != "") {
		return uc.repository.GetUsersWithPrefix(*prefix, searchOffset, searchLimit)
	} else {
		return uc.repository.GetUsers(searchOffset, searchLimit)
	}

}

func (uc *UseCaseImpl) GetUser(id string) (*model.User, *model.Error) {
	return uc.repository.GetUser(id)
}
