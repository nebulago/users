package usecase

import "gitlab.com/nebulago/users/pkg/domain/model"

func (uc *UseCaseImpl) UpdateUser(user *model.User) (*model.User, *model.Error) {
	if user == nil {
		return nil, &model.Error{Code: 400, Tag: "BAD_PARAMETERS", Message: "user is null"}
	}
	if user.UID == "" {
		return nil, &model.Error{Code: 400, Tag: "BAD_PARAMETERS", Message: "uid is null"}
	}
	if user.Name == "" {
		return nil, &model.Error{Code: 400, Tag: "BAD_PARAMETERS", Message: "name is null"}
	}
	if user.Email == "" {
		return nil, &model.Error{Code: 400, Tag: "BAD_PARAMETERS", Message: "email is null"}
	}
	return uc.repository.UpdateUser(user)
}
