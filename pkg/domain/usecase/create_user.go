package usecase

import "gitlab.com/nebulago/users/pkg/domain/model"

func (uc *UseCaseImpl) CreateUser(user *model.User, password string) (*model.User, *model.Error) {
	if user.Name == "" {
		user.Name = user.UID
	}
	return user, uc.repository.CreateUser(user, password)
}
