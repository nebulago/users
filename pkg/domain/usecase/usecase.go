package usecase

import "gitlab.com/nebulago/users/pkg/domain/model"

type UseCase interface {
	CreateUser(user *model.User, password string) (*model.User, *model.Error)
	GetUsers(prefix *string, offset *int, limit *int) (result *[]model.User, total int, err *model.Error)
	GetUser(id string) (*model.User, *model.Error)
	UpdateUser(user *model.User) (*model.User, *model.Error)
	DeleteUser(id string) *model.Error
}

type Repository interface {
	CreateUser(user *model.User, password string) *model.Error
	GetUsersWithPrefix(prefix string, offset int, limit int) (result *[]model.User, total int, err *model.Error)
	GetUsers(offset int, limit int) (result *[]model.User, total int, err *model.Error)
	GetUser(id string) (*model.User, *model.Error)
	UpdateUser(user *model.User) (*model.User, *model.Error)
	DeleteUser(id string) *model.Error
}

type UseCaseImpl struct {
	repository Repository
}

func NewUseCase(repo Repository) UseCase {
	return &UseCaseImpl{repository: repo}
}
