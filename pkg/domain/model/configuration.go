package model

type DatabaseConfiguration struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	DBName   string `json:"dbname"`
	User     string `json:"user"`
	Password string `json:"password"`
	SSLMode  string `json:"ssl"`
	Schema   string `json:"schema"`
}

type HttpConfiguration struct {
	Address string `json:"address"`
}

type Configuration struct {
	DatabaseConfiguration *DatabaseConfiguration `json:"database"`
	HttpConfiguration     *HttpConfiguration     `json:"http"`
}

type ConfigurationRepository struct {
	configuration *Configuration
}
