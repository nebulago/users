package model

type Error struct {
	Code    int    `json:"code"`
	Tag     string `json:"tag"`
	Message string `json:"message"`
}
