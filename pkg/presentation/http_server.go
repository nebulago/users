package presentation

import (
	"log"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/nebulago/users/pkg/domain/model"
	"gitlab.com/nebulago/users/pkg/domain/usecase"
)

type CreateUserRequest struct {
	UID      string `json:"uid" binding:"required"`
	Name     string `json:"name"`
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type GetUserListRequest struct {
	Prefix *string `form:"prefix"`
	Offset *int    `form:"offset"`
	Limit  *int    `form:"limit"`
}

type GetUsersResponse struct {
	Total int          `json:"total"`
	Users []model.User `json:"users"`
}

type HttpServer struct {
	configuration *model.HttpConfiguration
	usecase       usecase.UseCase
}

func NewHttpServer(configuration *model.HttpConfiguration, usecase usecase.UseCase) *HttpServer {
	return &HttpServer{
		configuration: configuration,
		usecase:       usecase,
	}
}

func (s *HttpServer) Start() error {
	log.Printf("starting server at %s\n", s.configuration.Address)
	router := gin.Default()
	router.Use(cors.Default())
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	v1user := router.Group("/v1/user")
	{
		v1user.POST("", s.createUser)
		v1user.GET("", s.getUserList)
		v1user.GET("/:id", s.getUser)
		v1user.PUT("", s.updateUser)
		v1user.DELETE("/:id", s.deleteUser)
	}
	return router.Run(s.configuration.Address)
}

func (s *HttpServer) createUser(c *gin.Context) {
	var request CreateUserRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		setHttpResponseResult(c, &model.Error{Code: 400, Tag: "INCORRECT_JSON"})
		return
	}
	user, err := s.usecase.CreateUser(&model.User{UID: request.UID, Name: request.Name, Email: request.Email}, request.Password)
	if err != nil {
		setHttpResponseResult(c, err)
		return
	}
	c.JSON(http.StatusCreated, user)
}

func (s *HttpServer) getUserList(c *gin.Context) {
	var request GetUserListRequest
	if err := c.Bind(&request); err != nil {
		setHttpResponseResult(c, &model.Error{Code: 400, Tag: "INCORRECT_JSON"})
		return
	}
	response, count, err := s.usecase.GetUsers(request.Prefix, request.Offset, request.Limit)
	if err != nil {
		setHttpResponseResult(c, err)
		return
	}
	c.JSON(http.StatusOK, &GetUsersResponse{Total: count, Users: *response})
}

func (s *HttpServer) getUser(c *gin.Context) {
	id := c.Param("id")
	response, err := s.usecase.GetUser(id)
	if err != nil {
		setHttpResponseResult(c, err)
		return
	}
	c.JSON(http.StatusOK, response)
}

func (s *HttpServer) updateUser(c *gin.Context) {
	var request model.User
	if err := c.ShouldBindJSON(&request); err != nil {
		setHttpResponseResult(c, &model.Error{Code: 400, Tag: "INCORRECT_JSON"})
		return
	}
	user, err := s.usecase.UpdateUser(&request)
	if err != nil {
		setHttpResponseResult(c, err)
		return
	}
	c.JSON(http.StatusOK, user)
}

func (s *HttpServer) deleteUser(c *gin.Context) {
	id := c.Param("id")
	err := s.usecase.DeleteUser(id)
	if err != nil {
		setHttpResponseResult(c, err)
		return
	}
	c.Status(http.StatusOK)
}

func setHttpResponseResult(c *gin.Context, err *model.Error) {
	c.JSON(err.Code, err)
}
