export class CreateUserRequest {
    uid: string;
    email: string;
    password: string;
}
