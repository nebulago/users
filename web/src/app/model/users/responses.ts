import { User } from './dto';

export class UserListResponse {
    users: User[];
    total: number;
}
