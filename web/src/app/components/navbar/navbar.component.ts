import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  currentItem: string;

  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.setCurrentItemFromPath(val.url);
      }
    });
  }

  setCurrentItemFromPath(path: string) {
    if (path.endsWith('/home')) {
      this.currentItem = 'home';
    } else if (path.endsWith('/users')) {
      this.currentItem = 'users';
    } else if (path.endsWith('/users/create')) {
      this.currentItem = 'createUser';
    }
  }

}
