import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/model/users/dto';
import { UserListResponse } from 'src/app/model/users/responses';
import { Router } from '@angular/router';
import { threatError } from 'src/app/utils/functions';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  userToSearch = '';

  usersToShow: User[];
  pagesToShow: number[];

  enabledPreviousPage: boolean;
  enabledNextPage: boolean;

  currentPage: number;
  totalPages: number;
  totalUsers: number;

  private elementsPerPage = 8;
  private pagesBeforeCurrent = 2;
  private pagesAfterCurrent = 2;

  constructor(private alertService: AlertService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    this.loadPage(0);
  }

  loadPage(page: number) {
    console.log('loading page ' + page);
    this.currentPage = page;
    const offset = page * this.elementsPerPage;
    this.userService.getUsers(this.userToSearch, offset, this.elementsPerPage)
      .subscribe(
        result => {
          this.updateElements(result);
        },
        error => {
          this.alertService.error(threatError(error));
        }
      );
  }

  updateElements(userList: UserListResponse) {
    this.totalUsers = userList.total;
    this.usersToShow = userList.users;
    this.enabledPreviousPage = this.currentPage > 0;
    this.enabledNextPage = userList.total > (this.elementsPerPage * this.currentPage) + userList.users.length;
    this.pagesToShow = [];
    this.totalPages = Math.ceil(userList.total / this.elementsPerPage);
    for (let i = 0; i < this.pagesBeforeCurrent; ++i) {
      if (this.currentPage - this.pagesBeforeCurrent + i >= 0) {
        this.pagesToShow.push(this.currentPage - this.pagesBeforeCurrent + i);
      }
    }
    this.pagesToShow.push(this.currentPage);
    for (let i = 0; i < this.pagesAfterCurrent; ++i) {
      if (this.currentPage + i + 1 < this.totalPages) {
        this.pagesToShow.push(this.currentPage + i + 1);
      }
    }
  }

  showUser(user: User) {
    this.router.navigate(['users/details', user.uid]);
  }

}
