import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/model/users/dto';
import { AlertService } from 'src/app/services/alert.service';
import { threatError } from 'src/app/utils/functions';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  user: User;

  name: string;
  email: string;

  deletingUser: boolean;

  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private alertService: AlertService) { }

  ngOnInit() {
    this.deletingUser = false;
    this.userService.getUser(this.route.snapshot.params.id)
      .subscribe(user => {
        this.updateFields(user);
      }, error => {
        this.alertService.error(threatError(error));
      });
  }

  modifyUser() {
    const modifiedUser = new User();
    modifiedUser.uid = this.user.uid;
    modifiedUser.name = this.name;
    modifiedUser.email = this.email;
    this.userService.modifyUser(modifiedUser)
      .subscribe(user => {
        this.alertService.success('Modified user ' + user.uid);
        this.updateFields(user);
      }, error => {
        this.alertService.error(threatError(error));
      });
  }

  updateFields(user: User) {
    this.user = user;
    this.name = user.name;
    this.email = user.email;
  }

  showModalDelete() {
    this.deletingUser = true;
  }

  cancelDeleteUser() {
    this.deletingUser = false;
  }

  confirmDeleteUser() {
    this.deletingUser = false;
    this.userService.deleteUser(this.user.uid)
      .subscribe(
        result => {
          this.alertService.success('User ' + this.user.uid + ' deleted');
        }, error => {
          this.alertService.error(threatError(error));
        }
      );
  }

}
