import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { UserService } from 'src/app/services/user.service';
import { CreateUserRequest } from 'src/app/model/users/requests';
import { threatError } from 'src/app/utils/functions';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  username: string;
  email: string;
  password: string;
  repeatPassword: string;

  constructor(private alertService: AlertService,
              private userService: UserService) { }

  ngOnInit() {
  }

  createUser() {
    if (this.password !== this.repeatPassword) {
      this.alertService.error('Password not match');
      return;
    }
    const request = new CreateUserRequest();
    request.email = this.email;
    request.uid = this.username;
    request.password = this.password;
    this.userService.createUser(request)
      .subscribe(
        result => this.alertService.success('User created'),
        error => this.alertService.error(threatError(error))
      );
  }
}
