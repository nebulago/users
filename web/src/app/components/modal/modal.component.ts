import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() id: string;
  @Input() title: string;
  @Input() body: string;
  @Input() buttonConfirmText: string;

  @Output() confirmed = new EventEmitter<any>();
  @Output() canceled = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  cancel() {
    this.canceled.emit();
  }

  confirm() {
    this.confirmed.emit();
  }

}
