export function threatError(error: any): string {
    console.log(error);
    if (error !== undefined && error !== null && error.error !== undefined && error.error !== null) {
        if (error.error.tag !== undefined && error.error.tag !== null) {
            return error.error.tag;
        }
    }
    return 'Unknown error';
}
