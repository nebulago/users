import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ConfigurationService } from './configuration.service';
import { CreateUserRequest } from '../model/users/requests';
import { User } from '../model/users/dto';
import { UserListResponse } from '../model/users/responses';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private serverAddress: string;

  headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient,
              private config: ConfigurationService) {
    this.serverAddress = config.serverAddress;
  }

  createUser(request: CreateUserRequest): Observable<User> {
    return this.http.post(`${this.serverAddress}/v1/user`, request, { headers: this.headers, observe: 'response' })
      .pipe(map(response => {
        return response.body as User;
      }));
  }

  getUsers(prefix: string, offset: number, elementsPerPage: number): Observable<UserListResponse> {
    return this.http.get(`${this.serverAddress}/v1/user`, {
      params: {
        prefix,
        limit: String(elementsPerPage),
        offset: String(offset),
      }, headers: this.headers, observe: 'response'
    })
      .pipe(map(response => {
        return response.body as UserListResponse;
      }));
  }

  getUser(id: string): Observable<User> {
    return this.http.get(`${this.serverAddress}/v1/user/${id}`, {
      headers: this.headers, observe: 'response'
    })
      .pipe(map(response => {
        return response.body as User;
      }));
  }

  modifyUser(modifiedUser: User): Observable<any> {
    return this.http.put(`${this.serverAddress}/v1/user`, modifiedUser, {
      headers: this.headers, observe: 'response'
    })
      .pipe(map(response => {
        return response.body as User;
      }));
  }

  deleteUser(id: string): Observable<any> {
    return this.http.delete(`${this.serverAddress}/v1/user/${id}`, {
      headers: this.headers, observe: 'response'
    });
  }

}
