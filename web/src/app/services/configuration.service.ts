import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  public serverAddress = 'http://localhost:8080';

  constructor() { }
}
