package main

import (
	"log"
	"os"

	"gitlab.com/nebulago/users/pkg/data"
	"gitlab.com/nebulago/users/pkg/domain/usecase"
	"gitlab.com/nebulago/users/pkg/presentation"
)

func main() {
	confFile := os.Getenv("configFile")
	if confFile == "" {
		confFile = "config.json"
	}
	jsonConfig, err := os.Open(confFile)
	if err != nil {
		log.Fatal(err.Error())
	}

	configRepo, err := data.NewConfigurationRepository(jsonConfig)
	if err != nil {
		log.Fatal(err.Error())
	}
	if err := jsonConfig.Close(); err != nil {
		log.Fatal(err.Error())
	}

	database, error := data.NewDatabase(configRepo.GetConfiguration().DatabaseConfiguration)
	if error != nil {
		log.Fatal(error.Message)
	}
	usecase := usecase.NewUseCase(database)

	server := presentation.NewHttpServer(configRepo.GetConfiguration().HttpConfiguration, usecase)

	if err = server.Start(); err != nil {
		log.Fatal(err.Error())
	}

}
