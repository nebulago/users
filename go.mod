module gitlab.com/nebulago/users

require (
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/gin-contrib/cors v0.0.0-20190301062745-f9e10995c85a
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/jinzhu/gorm v1.9.4
	github.com/jinzhu/now v1.0.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/kubernetes/kompose v1.18.0 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/satori/go.uuid v1.2.0
)
